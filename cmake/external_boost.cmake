#=================================================================================================#
# Download and build the boost library dependencies.                                              #
#=================================================================================================#

set( TARGET_NAME "boost" )
set( BOOST_VERSION "1.69.0" )
string( REGEX REPLACE "\\." "_" BOOST_UNDOTTED_VERSION ${BOOST_VERSION} )

set( BOOST_FILE_NAME "boost_${BOOST_UNDOTTED_VERSION}" )
set( BOOST_FILE "${BOOST_FILE_NAME}.tar.bz2" )
set( BOOST_URL "https://dl.bintray.com/boostorg/release/${BOOST_VERSION}/source/${BOOST_FILE}" )
set( BOOST_HASH "8f32d4617390d1c2d16f26a27ab60d97807b35440d45891fa340fc2648b04406" )


# Build settings
#==================================================================================================

# determine compiler toolset
if( MSVC )
    set( BOOST_TOOLSET "msvc")
elseif( CMAKE_CXX_COMPILER_ID MATCHES "Clang" )
    set( BOOST_TOOLSET "clang" )
elseif( CMAKE_CXX_COMPILER_ID MATCHES "GNU" )
    set( BOOST_TOOLSET "gcc" )
else()
    message( FATAL_ERROR "Compiler not supported." )
endif()
message( STATUS "Boost toolset set to ${BOOST_TOOLSET}." )


# determine bitness
if( CMAKE_SIZEOF_VOID_P EQUAL 8 )
    set( BOOST_BITNESS 64 )
elseif( CMAKE_SIZEOF_VOID_P EQUAL 4 )
    set( BOOST_BITNESS 32 )
else()
    message( FATAL_ERROR "Failed to determine compile bitness." )
endif()
message( STATUS "Boost bitness set to ${BOOST_BITNESS}." )


# determine debug + release compile flags
set( BOOST_C_FLAGS_DEBUG "${CMAKE_C_FLAGS} ${CMAKE_C_FLAGS_DEBUG}" )
set( BOOST_CXX_FLAGS_DEBUG "${CMAKE_CXX_FLAGS} ${CMAKE_CXX_FLAGS_DEBUG}" )
set( BOOST_C_FLAGS_RELEASE "${CMAKE_C_FLAGS} ${CMAKE_C_FLAGS_RELEASE}" )
set( BOOST_CXX_FLAGS_RELEASE "${CMAKE_CXX_FLAGS} ${CMAKE_CXX_FLAGS_RELEASE}" )

if( MSVC )
    # adjust to avoid conflict warnings with boost internal compile flags
    # to avoid "warning D9025 : overriding '/W3' with '/W4'"
    # let boost build keep the desired lower warning level
    string( REPLACE " /W4" "" BOOST_C_FLAGS_DEBUG ${BOOST_C_FLAGS_DEBUG} )
    string( REPLACE " /W4" "" BOOST_CXX_FLAGS_DEBUG ${BOOST_CXX_FLAGS_DEBUG} )
    string( REPLACE " /W4" "" BOOST_C_FLAGS_RELEASE ${BOOST_C_FLAGS_RELEASE} )
    string( REPLACE " /W4" "" BOOST_CXX_FLAGS_RELEASE ${BOOST_CXX_FLAGS_RELEASE} )
    # to avoid "warning D9025 : overriding '/Z7' with '/Zi'"
    # let boost keep the desired /Z7 flag to embed the debug symbols to the object files
    # /Zi instead generates *.pdb files
    string( REPLACE " /Zi" "" BOOST_C_FLAGS_DEBUG ${BOOST_C_FLAGS_DEBUG} )
    string( REPLACE " /Zi" "" BOOST_CXX_FLAGS_DEBUG ${BOOST_CXX_FLAGS_DEBUG} )
endif()

message( STATUS "BOOST_C_FLAGS_DEBUG: ${BOOST_C_FLAGS_DEBUG}" )
message( STATUS "BOOST_CXX_FLAGS_DEBUG: ${BOOST_CXX_FLAGS_DEBUG}" )
message( STATUS "BOOST_C_FLAGS_RELEASE: ${BOOST_C_FLAGS_RELEASE}" )
message( STATUS "BOOST_CXX_FLAGS_RELEASE: ${BOOST_CXX_FLAGS_RELEASE}" )


# boost build arguments
# refer https://www.boost.org/doc/libs/1_67_0/doc/html/bbv2/reference.html
# note that the c compile flags are also passed to the c++ compiler, see https://www.boost.org/doc/libs/1_67_0/doc/html/bbv2/reference.html
set( B2_ARGS_ANY
    toolset=${BOOST_TOOLSET}                    # compiler toolset
    architecture=x86                            # x86
    address-model=${BOOST_BITNESS}              # 64 or 32
    link=static                                 # build static libraries
    runtime-link=static                         # statically link c++ runtime library
    threading=multi                             # enable multithreaded awareness
    --with-system                               # build system library required by Boost.Asio
    --with-program_options                      # build Boost.Program_options to parse the commandline
#    --layout=versioned                         # output versioned library names not working with gcc, see https://github.com/boostorg/build/issues/273
)

if( CMAKE_HOST_APPLE )
    set( B2_ARGS_ANY ${B2_ARGS_ANY}
        --with-filesystem                       # XCode clang sadly still is missing c++17 std::filesystem
    )
endif()

set( B2_ARGS_DEBUG ${B2_ARGS_ANY}
    variant=debug                               # debug build
    cflags=${BOOST_C_FLAGS_DEBUG}               # pass on the c compile flags ( also passed to c++ compiler, see https://www.boost.org/doc/libs/1_67_0/doc/html/bbv2/reference.html )
    cxxflags=${BOOST_CXX_FLAGS_DEBUG}           # pass on the c++ compile flags
)

set( B2_ARGS_RELEASE ${B2_ARGS_ANY}
    variant=release                             # release build
    cflags=${BOOST_C_FLAGS_RELEASE}             # pass on the c compile flags ( also passed to c++ compiler, see https://www.boost.org/doc/libs/1_67_0/doc/html/bbv2/reference.html )
    cxxflags=${BOOST_CXX_FLAGS_RELEASE}         # pass on the c++ compile flags
)

# generator expressions for msvc multi-configuration setup support
# they are re-evaluated each build so it doesn't mess the cache
set( B2_ARGS_GEN ${B2_ARGS_ANY}
    variant=$<$<CONFIG:Debug>:debug>$<$<NOT:$<CONFIG:Debug>>:release>
    cflags=$<$<CONFIG:Debug>:${BOOST_C_FLAGS_DEBUG}>$<$<NOT:$<CONFIG:Debug>>:${BOOST_C_FLAGS_RELEASE}>
    cxxflags=$<$<CONFIG:Debug>:${BOOST_CXX_FLAGS_DEBUG}>$<$<NOT:$<CONFIG:Debug>>:${BOOST_CXX_FLAGS_RELEASE}>
)

# select boost build args via CMAKE_BUILD_TYPE
if( NOT CMAKE_BUILD_TYPE )
    message( STATUS "Build Boost debug and release variants." ) # used for msvc multi-configuration setup
    set( B2_ARGS ${B2_ARGS_GEN} )
elseif( CMAKE_BUILD_TYPE MATCHES "Debug" )
    message( STATUS "Build Boost debug variant." )
    set( B2_ARGS ${B2_ARGS_DEBUG} )
else()
    message( STATUS "Build Boost release variant." )
    set( B2_ARGS ${B2_ARGS_RELEASE} )
endif()
message( STATUS "Boost B2_ARGS: ${B2_ARGS}" )


# Download
#==================================================================================================

set( BOOST_DEPS_DIR "${CMAKE_CURRENT_BINARY_DIR}/deps/boost" )      # boost project subdirectory
set( BOOST_DOWNLOAD_FILE "${BOOST_DEPS_DIR}/${BOOST_FILE}" )        # boost download file path

file( DOWNLOAD
    ${BOOST_URL}                                # download url
    ${BOOST_DOWNLOAD_FILE}                      # file download path
    EXPECTED_HASH SHA256=${BOOST_HASH}          # download checksum
    SHOW_PROGRESS
)


# Target project
# use custom download & unpack instead of the ExternalProject_Add version
# to not overwrite but reuse extracted code in multi-configuration setup like with MSVC
#==================================================================================================

set( BOOST_UNPACK_DIR "${BOOST_DEPS_DIR}/${BOOST_FILE_NAME}" )  # temp unzip dir
set( BOOST_SOURCE_DIR "${BOOST_DEPS_DIR}/src" )                 # boost build directory
message( STATUS "BOOST_SOURCE_DIR: ${BOOST_SOURCE_DIR}" )

# rename unpacked boost download execution command
set( RENAME_EXEC "\"${CMAKE_COMMAND}\" -E rename \"${BOOST_UNPACK_DIR}\" \"${BOOST_SOURCE_DIR}\"" )
#set( RENAME_EXEC "${CMAKE_COMMAND} -E rename \"${BOOST_UNPACK_DIR}\" \"${BOOST_SOURCE_DIR}\"" )

# figure platform dependent unpack and bootstrap commands
# cmake expects to run some application or script and await the execution result
# therefore execute indirectly via command shell to not fail proceed with onfollowing commands
# to show unpack progress, change UNPACK_EXEC tar -xzf to -xzvf option, this however prints all the many boost files extracted
# \todo reattempt with VERBATIM flag to possibly simplify the escaping
if( CMAKE_HOST_WIN32 )
    # unpack execution
    set( UNPACK_INFO "\"${CMAKE_COMMAND}\" -E echo \"unpacking boost to \\\"${BOOST_SOURCE_DIR}\\\"...\"" )
    set( UNPACK_EXEC "${UNPACK_INFO} && \"${CMAKE_COMMAND}\" -E tar -xzf \"${BOOST_DOWNLOAD_FILE}\"" )

    # unpack boost if missing files
    set( UNPACK_CMD cmd /c \"if not exist \"${BOOST_SOURCE_DIR}/boost\" ${UNPACK_EXEC}\" )

    # rename temp boost unzip dir if found
    set( RENAME_CMD cmd /c "if exist \"${BOOST_UNPACK_DIR}\" ${RENAME_EXEC}" )

    # bootstrap if not already
    set( BOOTSTRAP_CMD cmd /c \"if not exist b2.exe bootstrap.bat\" )
else()
    # unpack execution
    set( UNPACK_INFO "${CMAKE_COMMAND} -E echo \"unpacking boost to \\\\\\\"${BOOST_SOURCE_DIR}\\\\\\\"...\"" )
    set( UNPACK_EXEC "${UNPACK_INFO} \\&\\& ${CMAKE_COMMAND} -E tar -xzf \"${BOOST_DOWNLOAD_FILE}\"" )

    # unpack boost if missing files, return true to proceed
    set( UNPACK_CMD bash -c "[ -d \"${BOOST_SOURCE_DIR}\" ] \\|\\| \\( ${UNPACK_EXEC} \\) \\|\\| true" )
    set( RENAME_CMD bash -c "[ -d \"${BOOST_UNPACK_DIR}\" ] \\&\\& \\( ${RENAME_EXEC} \\) \\|\\| true" )

    # bootstrap if not already
    set( BOOTSTRAP_CMD bash -c "[ -x ./b2 ] \\|\\| pwd \\&\\& ./bootstrap.sh" )
endif()

message( STATUS "UNPACK_CMD: ${UNPACK_CMD}" )
message( STATUS "RENAME_CMD: ${RENAME_CMD}" )
message( STATUS "BOOTSTRAP_CMD: ${BOOTSTRAP_CMD}" )


# use custom_command naming the source output directory so cmake caches to not rerun
# however double-check to not re-unpack and -bootstrap for the MSVC multi-configuration setup
add_custom_command(
    OUTPUT ${BOOST_SOURCE_DIR}
    COMMENT "Checking to unpack the boost build..."
    WORKING_DIRECTORY "${BOOST_DEPS_DIR}"
    COMMAND ${UNPACK_CMD}                                   # unpack boost creating the BOOST_UNPACK_DIR directory
    COMMAND ${CMAKE_COMMAND} -E echo "boost unpacking done"
    COMMAND ${RENAME_CMD}                                   # rename BOOST_UNPACK_DIR to BOOST_SOURCE_DIR
    COMMAND ${CMAKE_COMMAND} -E echo "boost renaming done"
)

# bootstrap by second command to adjust the working directory
add_custom_command(
    # note that on windows the OUTPUT directory is auto-generated ahead of executing the other custom commands
    # therefore naming the BOOST_SOURCE_DIR breaks the unpack file rename
    # instead touch to generate some empty stamp file to check
    OUTPUT ${BOOST_DEPS_DIR}/bootstrap.stamp
    WORKING_DIRECTORY "${BOOST_SOURCE_DIR}"                 # navigate to the unpacked src directory

    COMMENT "Checking to bootstrap the boost build..."
    COMMAND ${BOOTSTRAP_CMD}                                # bootstrap the boost build environment
    COMMAND touch "${BOOST_DEPS_DIR}/bootstrap.stamp"       # generate empty stamp file to skip next build
    COMMAND ${CMAKE_COMMAND} -E echo "boost bootstrapping done"
)

# build by third command to not depend on the bootstrapping and improve visual cmake progress
add_custom_command(
    # note that on windows the OUTPUT directory is auto-generated ahead of executing the other custom commands
    # therefore naming the BOOST_SOURCE_DIR breaks the unpack file rename
    # instead touch to generate some empty stamp file to check
    OUTPUT ${BOOST_DEPS_DIR}/build.stamp
    WORKING_DIRECTORY "${BOOST_SOURCE_DIR}"                 # navigate to the unpacked src directory

    COMMENT "Initiating boost build..."
    COMMAND ./b2 ${B2_ARGS}                                 # call b2 to build boost
    COMMAND touch "${BOOST_DEPS_DIR}/build.stamp"           # generate empty stamp file to skip next build
    COMMAND ${CMAKE_COMMAND} -E echo "boost build done"
)

#add_custom_command(
#    OUTPUT ${BOOST_DEPS_DIR}/test
#    COMMENT "test..."
#    WORKING_DIRECTORY "${BOOST_DEPS_DIR}" # navigate to the unpacked src directory
#    COMMAND ./b2 ${B2_ARGS}                 # call b2 to build boost
#    COMMAND ${CMAKE_COMMAND} -E echo "boost build done"
#)

# unpack, bootstrap and build boost
add_custom_target( ${TARGET_NAME} DEPENDS
    ${BOOST_SOURCE_DIR}
    ${BOOST_DEPS_DIR}/bootstrap.stamp
    ${BOOST_DEPS_DIR}/build.stamp
#    ${BOOST_DEPS_DIR}/test
)


# Set BOOST_ROOT
#==================================================================================================

set( BOOST_ROOT "${BOOST_SOURCE_DIR}" )     # set BOOST_ROOT variable for Find_Boost to find the library path
