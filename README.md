UrlCheck - ©2019 Hannes Ahrens - irrlabs.de
===========================================

UrlCheck repeatedly checks a given website address for the http status code.
It prints to both the console and a log file whether the status code is OK so the site can be reached.
It further is multiligual ( german + english ) and runs on Windows, Linux and Mac.

To run open a command prompt and simply pass the url to check.
Be aware that the https protocol is not supported and any status code other than 'OK' ( like a redirect ) is interpreted as a miss.
In my testing I further noticed some websites to fail generate an appropriate status code and just return OK but generate some error page for an non-existent address. I didn't proceed to investigate this any further.

As additional parameters you can pass the logfile path, seconds for timeout and seconds for interval, like e.g.:
```
./url-check www.example.com ./test.log 1 1
```
For additional parameters, call:
```
./url-check -h
```

It is just a small cmake cross-platform sample application I implemented, but to my believe it has a nice utilization of different C++ programming aspects and at least for myself I'm sure will proove well as a template.

Specially the CMake cross-platform setup often is tricky.
Here it is setup with a super project to automatically download and compile the boost library.
Further it is configured for debug + release multi-configuration build like with MS Visual Studio.
The boost unpacking and bootstrapping is shared and also the compilation is only triggered once per configuration.
The main app project is always run so changes are also detected when building the super project.

Further at least for Windows it includes to generate version information for the details tab of the application preferences.


# Windows Build

## Prerequisites

install Visual Studio 2017 with C++ desktop app components
install CMake >= 3.9.6 ( download from https://cmake.org/install/ )

Other compilers like MinGW not yet tested, but likely have no multi-configuration support.


## Configure

create and navigate to build subdirectory
```
mkdir build && cd build
```

configure to build parent folder for Visual Studio x64 multi-configuration setup
```
cmake -G "Visual Studio 15 2017 Win64" ..
```


## Build

compile building the ALL_BUILD target
select either Release or Debug configuration, they share the same build directory
to change the executable build path, add -DRUNTIME_OUTPUT_DIRECTORY=<path> to the build command
by default it is set to ./build
```
cmake --build . --config Release --target ALL_BUILD
```


## Build & Install

install building the INSTALL target
to change the install path, add -DINSTALL_PREFIX=<path> to the build command
by default it is set to ./install
```
cmake --build . --config Release --target INSTALL
```


# Linux Build

## Prerequisites

check that
``cmake --version`` is >= 3.9.6 ( download from https://cmake.org/install/ )
``gcc -v`` is >= 8.0.0
``g++ -v`` is >= 8.0.0

if not update with
```
sudo apt-get install cmake
sudo apt-get install gcc-8 g++-8
```

change default gcc version to gcc-8 with
```
update-alternatives --install /usr/bin/gcc gcc /usr/bin/gcc-8 100 --slave /usr/bin/g++ g++ /usr/bin/g++-8
update-alternatives --config gcc
```


## Configure

create and navigate to build subdirectory
```
mkdir release && cd release
```

configure to build parent folder as either release or debug build
```
cmake -DCMAKE_BUILD_TYPE:STRING=Release ..
```


## Build

compile building the ALL_BUILD target
to change the executable build path, add -DRUNTIME_OUTPUT_DIRECTORY=<path> to the build command
by default it is set to ./build
```
cmake --build .
```


## Build & Install

install building the INSTALL target
to change the install path, add -DINSTALL_PREFIX=<path> to the build command
by default it is set to ./install
```
cmake --build . --target INSTALL
```


# macOS Build

## Prerequisites

check that
``cmake --version`` is >= 3.9.6 ( download from https://cmake.org/install/ )
``clang -v`` is >= Apple LLVM version 10.0.0 (clang-1000.10.44.4)


## Configure

create and navigate to build subdirectory
```
mkdir release && cd release
```

configure to build parent folder as either release or debug build
```
cmake -DCMAKE_BUILD_TYPE:STRING=Release ..
```


## Build

compile building the ALL_BUILD target
to change the executable build path, add -DRUNTIME_OUTPUT_DIRECTORY=<path> to the build command
by default it is set to ./build
```
cmake --build .
```


## Build & Install

install building the INSTALL target
to change the install path, add -DINSTALL_PREFIX=<path> to the build command
by default it is set to ./install
```
cmake --build . --target INSTALL
```

