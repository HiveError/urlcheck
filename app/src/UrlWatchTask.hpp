#pragma once

#include "UrlStatusTask.hpp"

#include <boost/beast/core.hpp>   // flat_buffer
#include <boost/asio/ip/tcp.hpp>
#include <boost/beast/http.hpp>

#include <chrono>
#include <condition_variable>
#include <fstream>
#include <mutex>
#include <string>
#include <thread>

#ifdef __APPLE__
    #include <boost/filesystem.hpp>
    namespace fs = boost::filesystem;
#else
    #include <filesystem>
    namespace fs = std::filesystem;
#endif


namespace url_check {

    /**
    * \brief UrlWatchTask to perform the url check.
    */
    class UrlWatchTask : public Task
    {
    public:
        /**
        * \brief Task constructor initializing it ready for being processed.
        * \param addr           The web address to check.
        * \param timeout        Timeout for url response in seconds.
        * \param interval       Fixed interval for successive testing in seconds.
        * \param logpath        Optional logfile path to log the url status result.
        */
        UrlWatchTask( const WebAddress& addr, std::chrono::seconds timeout, std::chrono::seconds interval, fs::path logpath = "" );
        virtual ~UrlWatchTask();

        /**
        * \brief loops to perform the url checking until it is canceled
        */
        virtual void run() override;

        /**
        * \brief cancel the url check
        */
        virtual void cancel() override;

    private:
        /// \brief callback for the UrlStatusTask status
        void _onStatus( boost::beast::http::status status );
        /// \brief callback for the UrlStatusTask error handler
        void _onStatusError( const std::string& error );

    private:
        std::string                     m_url;               // WebAddress url, used for logging
        std::chrono::seconds            m_timeout;           // timeout for the probing response
        std::chrono::seconds            m_interval;          // fixed interval to determine the next probing time
        fs::path                        m_logpath;
        std::fstream                    m_logfile;

        UrlStatusTask                   m_statusTask;        // task to request the url status
        std::thread                     m_taskRunner;        // \todo task runner thread, could be advanced to some class handling all the different tasks

        bool                            m_quit = 0;          // Signals to quit. No need for an atomic here, any value change signals to quit.
        std::condition_variable         m_cv_timer;          // Condition variable to wait for timeout and signal to quit.
        std::mutex                      m_cvm_timer;         // Mutex to wait on the condition variable.

        std::atomic<bool>               m_inProcess = false; // flag for whether the url checking is active, updated by m_taskRunner
        std::atomic<bool>               m_success = false;   // url status result, updated by m_taskRunner
    };

} // namespace url_check
