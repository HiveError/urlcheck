#pragma once

#include <chrono>
#include <string>


namespace url_check {

struct Options
{
    std::string          url;      // the url address to watch, see UrlChecker
    std::string          logfile;  // url probing logfile
    std::chrono::seconds timeout;  // url probing timeout, see UrlChecker
    std::chrono::seconds interval; // url probing interval, see UrlChecker
    std::string          lang;     // console and log language, see Dict + Dict_Mappings

    /// parses options from commandline parameters
    bool parse_commanline( int argc, char** argv );
};

} // namespace url_check
