#include "UrlChecker.hpp"
#include "UrlWatchTask.hpp"
#include "Dict.hpp"

#include <iostream> // cout


namespace url_check {

UrlChecker::~UrlChecker()
{
    // make sure the inner threads are joined
    stop();
}

void UrlChecker::watch( const std::string& addr, std::chrono::seconds timeout, std::chrono::seconds interval, const std::string& logfile_path )
{
    url_check::WebAddress parsed_addr;
    parsed_addr.parseUrl( addr );
    watch( parsed_addr, timeout, interval, logfile_path );
}

void UrlChecker::watch( const WebAddress& addr, std::chrono::seconds timeout, std::chrono::seconds interval, const std::string& logfile_path )
{
    // stop if already running
    stop();

    // check whether to enable file logging
    bool logging = logfile_path.size();
    if( logging )
    {
        try
        {
            // ask back if the file already exists
            if( fs::exists( logfile_path ) )
            {
                if( !fs::is_regular_file( logfile_path ) )
                {
                    std::cout << "Skipping to log to \"" << logfile_path << "\", path exists but is no regular file." << std::endl;
                    logging = false;
                }
                else
                {
                    std::cout << Dict::lookup( "ask_log_overwrite" ) << std::endl;
                    const auto c = getchar();
                    if( c != 'y' )
                    {
                        logging = false;
                    }

                    // ignore remaining characters on the input stream
                    // getchar() & cin.get() not even remove the new line character
                    std::cin.ignore( std::numeric_limits<std::streamsize>::max(), '\n' );
                }
            }
        }
        catch( const std::exception& e )
        {
            std::cerr << "Failed to enable file logging, error: " << e.what() << std::endl;
            logging = false;
        }
    }

    // create new url check task
    m_task = std::make_shared<UrlWatchTask>( addr, timeout, interval, logging ? logfile_path : "" );
    // start task processing, keeping a copy of the task shared_ptr
    m_taskRunner = std::thread( [task = m_task](){ task->run(); } );
}

void UrlChecker::stop()
{
    if( m_taskRunner.joinable() )
    {
        if( m_task )
        {
            m_task->cancel();
        }
        m_taskRunner.join();
    }
}

} // namespace url_check
