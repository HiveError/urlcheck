#pragma once

#include "WebAddress.hpp"

#include <chrono>
#include <memory>
#include <string>
#include <thread>


namespace url_check {

    // forward declare to skip unnecessary inclusion
    class UrlWatchTask;

    /**
    * \brief UrlChecker class to test for url availability.
    *        Sends a head only request and checks the returned status code if not timed out.
    */
    class UrlChecker
    {
    public:
        UrlChecker() = default;
        ~UrlChecker();

        /**
        * \brief Launches a thread starting to test the given url status code for availability.
        *        When already run, the current task first is stopped.
        *        The result is print both to console and to the named logfile.
        * \param addr           The web address to check.
        * \param timeout        Timeout for url response in seconds.
        * \param interval       Fixed interval for successive testing in seconds.
        * \param logfile_path   Optional abolute log file path.
        */
        void watch( const std::string& addr, std::chrono::seconds timeout, std::chrono::seconds interval, const std::string& logfile_path = "" );
        void watch( const WebAddress& addr, std::chrono::seconds timeout, std::chrono::seconds interval, const std::string& logfile_path = "" );

        /**
        * \brief Stops and joins the url testing thread.
        */
        void stop();

    private:
        // Thread to run the url check task.
        std::thread              m_taskRunner;
        // The url check task to process.
        std::shared_ptr<UrlWatchTask> m_task;
    };

} // namespace url_check
