#pragma once

#include <string>


namespace url_check {

    struct WebAddress
    {
        std::string protocol; // "http"
        std::string host;     // "www.example.com"
        std::string target;   // "/index.html"

        /// parse from url string, e.g. "http//:www.example.com/index.html"
        void parseUrl( const std::string& url );
        /// convert to url string, e.g. "http//:www.example.com/index.html"
        std::string toUrl() const;
    };

} // namespace url_check
