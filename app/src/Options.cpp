#include "Options.hpp"
#include "Dict.hpp"

#include <boost/program_options.hpp>
#include <iostream>

#ifdef _WIN32
    #include <conio.h> // _getch
    #define _getch getch;
#else
    #include <termios.h>
    #include <unistd.h>
    int getch(void)
    {
        termios io_old;
        tcgetattr( STDIN_FILENO, &io_old );
        termios io_new( io_old );
        io_new.c_lflag &= ~(ICANON|ECHO);

        tcsetattr( STDIN_FILENO, TCSANOW, &io_new );
        int c = getchar();
        tcsetattr( STDIN_FILENO, TCSANOW, &io_old );

        return c;
    }
#endif

namespace po = boost::program_options;

#ifdef _WIN32
    #define DEFAULT_LOGFILE "url-check.log"
#else
    #define DEFAULT_LOGFILE "./url-check.log"
#endif

namespace url_check {

bool Options::parse_commanline( int argc, char** argv )
{
    po::options_description desc( "UrlCheck commandline options" );
    desc.add_options()
        ("help,h",                                                                  "this help message")
        ("url,u",      po::value<std::string>(),                                    "url to watch")
        ("log,l",      po::value<std::string>()->default_value( DEFAULT_LOGFILE ),  "logfile path (optional, set \"\" to disable)")
        ("timeout,t",  po::value<unsigned int>()->default_value( 10 ),              "response timeout in seconds (optional)")
        ("interval,i", po::value<unsigned int>()->default_value( 30 ),              "interval for repeation in seconds (optional)")
        ("lang",       po::value<std::string>(),                                    "language (optional)");

    // match positional options to allow skip naming the above parameters
    po::positional_options_description pos_desc;
    pos_desc.add( "url", 1 );      // first
    pos_desc.add( "log", 1 );      // second
    pos_desc.add( "timeout", 1 );  // third
    pos_desc.add( "interval", 1 ); // fourth

    if( argc < 2 )
    {
        // optionally prompt the user with std::cin
        std::cout << Dict::lookup( "missing_arguments" ) << std::endl;
        std::cout << desc << std::endl;
        std::cout << Dict::lookup( "quit_by_key" ) << std::endl;

        // wait for some key being pressed
        getch();
        return false;
    }

    auto parsed = po::command_line_parser( argc, argv ).options( desc ).positional( pos_desc ).run();

    po::variables_map vm;
    po::store( parsed, vm );
    po::notify( vm );

    if( vm.count( "help" ) )
    {
        std::cout << desc << std::endl;
        // no need to wait when called from a console
        return false;
    }
    if( !vm.count( "url" ) )
    {
        std::cout << "Missing the url to watch." << std::endl;
        std::cout << desc << std::endl;
        return false;
    }

    this->url = vm["url"].as<std::string>();
    this->logfile = vm.count( "log" ) ? vm["log"].as<std::string>() : "";
    this->timeout = std::chrono::seconds( vm["timeout"].as<unsigned int>() ); // defaulted via options_description
    this->interval = std::chrono::seconds( vm["interval"].as<unsigned int>() ); // defaulted via options_description

    if( vm.count( "lang" ) )
    {
        this->lang = vm["lang"].as<std::string>();
    }
    return true;
}

} // namespace url_check
