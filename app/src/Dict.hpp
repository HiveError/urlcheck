#pragma once

#include <map>
#include <string>


namespace url_check {

    using LanguageMap_base = std::map<std::string, std::string>;

    /**
    * \brief Language definition with key-value mapping.
    */
    class LanguageMap : public LanguageMap_base
    {
        typedef LanguageMap_base base_type;
        typedef base_type::value_type value_type;

    public:
        /// language code, e.g. 'en'
        const std::string language;
        /// country code, e.g. 'US'
        const std::string country;

    public:
        LanguageMap( LanguageMap&& ) = default;
        LanguageMap( const LanguageMap& ) = default;
        LanguageMap( std::string lang_, std::string country_, std::initializer_list<value_type> il )
            : base_type( std::move( il ) ), language( std::move( lang_ ) ), country( std::move( country_ ) ) { }

        /**
        * \brief Lookup key value.
        *
        * \param key    the key to lookup
        * \return       the mapped value or ""
        */
        std::string lookup( const std::string& key ) const;
    };

    using LangIndex_base = std::map<std::string, const LanguageMap&>;

    /**
    * \brief Language index mapping the LanguageMaps by reference
    *        to allow list languages multiple times for different keys.
    */
    class LangIndex : public LangIndex_base
    {
        typedef LangIndex_base base_type;
        typedef base_type::value_type value_type;

    public:
        LangIndex( LangIndex&& ) = default;
        LangIndex( const LangIndex& ) = default;
        LangIndex( std::initializer_list<value_type> il ) : base_type( std::move( il ) ) { }

        /// \return std::locale language code
        std::string getLangCode( const std::string& lang ) const;

        /**
        * \brief Lookup key value for the given lang.
        *
        * \param lang   the language to lookup
        * \param key    the key to lookup
        * \return       the language mapped value or ""
        */
        std::string lookup( const std::string& lang, const std::string& key ) const;
    };

    /**
    * \brief Dictionary wrapping the LangIndex lookup for static access,
    *        for general language configuration and for the default language fallback.
    */
    class Dict
    {
    public:
        static inline bool        hasLanguage( const std::string& lang )    { return m_index.find( lang ) != m_index.end(); }
        static inline std::string getLanguage()                             { return m_lang; }
        static std::string        setLanguage( std::string lang );
        static inline std::string getDefaultLanguage()                      { return m_default_lang; }
        static inline std::string setDefaultLanguage( std::string lang )    { std::swap( m_default_lang, lang ); return lang; }

        /// \return std::locale language code, defaults to the default language set
        static inline std::string getLangCode()                             { return getLangCode( m_lang ); }
        static std::string        getLangCode( const std::string& lang );

        /// \return system language in lower case, e.g. 'de' or 'german'
        static std::string        getSystemLanguage();

        /**
        * \brief Lookup key value for the set language.
        *        Defaults to default_lang if not found.
        *
        * \param key    the key to lookup
        * \return       the language mapped value or ""
        */
        static inline std::string lookup( const std::string& key )       { return m_index.lookup( m_lang, key ); }

        /**
        * \brief Lookup key value for the given lang.
        *        Defaults to default_lang if not found.
        *
        * \param lang   the desired language
        * \param key    the key to lookup
        * \return       the language mapped value or "<key>"
        */
        static std::string        lookup( const std::string& lang, const std::string& key );

    private:
        // !!! the languege here is only set during main initialization,
        // otherwise some locking mechanism is required !!!
        static std::string m_default_lang;
        static std::string m_lang;
        static const LangIndex m_index;
    };

} // namespace url_check
