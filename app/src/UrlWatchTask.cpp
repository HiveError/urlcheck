#include "UrlWatchTask.hpp"
#include "Dict.hpp"

#include <boost/beast/version.hpp>
#include <boost/asio/connect.hpp>

#include <algorithm>          // transform
#include <atomic>
#include <cctype>             // toupper
#include <iomanip>            // put_time
#include <iostream>           // cout
#include <sstream>            // stringstream

namespace url_check {

namespace {

    /**
    * \brief time point conversion to match "15-SEP-2011 14:00:00" format
    */
    std::string time_string( const std::chrono::time_point<std::chrono::system_clock>& tp )
    {
        std::time_t ttime = std::chrono::system_clock::to_time_t( tp );

        struct tm lotime;
#ifdef _WIN32
        localtime_s( &lotime, &ttime );
#else
        localtime_r( &ttime, &lotime );
#endif

        std::stringstream sstream;
        sstream << std::put_time( &lotime, "%d-%b-%Y %H:%M:%S" );
        auto stime = sstream.str();

        // convert to upper case
        std::transform( stime.begin(), stime.end(), stime.begin(), []( char c ) { return static_cast<char>(std::toupper( c )); } );

        return stime;
    }

} // anonymous namespace


UrlWatchTask::UrlWatchTask( const WebAddress& addr, std::chrono::seconds timeout, std::chrono::seconds interval, fs::path logpath )
    : m_url( addr.toUrl() ), m_timeout( timeout ), m_interval( interval ), m_logpath( std::move( logpath ) )
    , m_statusTask( std::move( addr ), std::bind( &UrlWatchTask::_onStatus, this, std::placeholders::_1 ), std::bind( &UrlWatchTask::_onStatusError, this, std::placeholders::_1 ) )
{
    // open or create logfile when the path is given
    if( !m_logpath.empty() )
    {
        try
        {
            m_logfile.open( m_logpath.string(), std::fstream::out | std::fstream::app );
        }
        catch( const std::exception& e )
        {
            std::cerr << "Failed to open the logfile by error: " << e.what() << std::endl;
        }
    }
}

UrlWatchTask::~UrlWatchTask()
{
    // make sure the processing is quit
    cancel();
}

void UrlWatchTask::run()
{
    m_quit = false;

    auto ts_next = std::chrono::system_clock::now() + m_interval;

    // lock mutex to not miss any notifications
    std::unique_lock<std::mutex> lock( m_cvm_timer );

    try
    {
        // run timeout loop
        while( !m_quit )
        {
            m_inProcess = true;
            m_success = false;

            // join task runner prior to starting another thread
            if( m_taskRunner.joinable() )
            {
                m_taskRunner.join();
            }

            // issue url testing in some async thread
            // reuse existing task and directly call the task run method,
            // the lifetime is assured to persist till task runner is joined
            m_taskRunner = std::thread( std::bind( &UrlStatusTask::run, &m_statusTask ) );

            // wait for url status or timeout to occure
            m_cv_timer.wait_for( lock, m_timeout, [this]() { return m_quit || !m_inProcess; } );
            if( m_quit )
            {
                return;
            }

            auto ts_now = std::chrono::system_clock::now();
            bool timed_out = m_inProcess;
            bool failed = timed_out || !m_success;

            // cancel async requests
            m_statusTask.cancel();

            // logging
            {
                std::string time = time_string( ts_now );
                std::stringstream logmsg;
                logmsg << time << ' ' << Dict::lookup( "url_time" ) << ": " << m_url << " -> "
                    << ( failed ? Dict::lookup( "url_not_reachable" ) : Dict::lookup( "url_reachable" ) ) << '!';

                std::cout << logmsg.str() << std::endl;

                try
                {
                    if( m_logfile.is_open() )
                    {
                        m_logfile << logmsg.str() << std::endl;
                    }
                }
                catch( const std::exception& e )
                {
                    std::cerr << "Failed to log to the logfile with error: " << e.what() << std::endl;
                }
            }

            // require at least 100ms pause for the next timeout
            auto ts_min = ts_now + std::chrono::milliseconds( 100 );

            if( ts_next > ts_min )
            {
                // wait for the timeout to elapse
                m_cv_timer.wait_until( lock, ts_next, [this]() { return m_quit; } );
                if( m_quit )
                {
                    return;
                }
            }
            else
            {
                // adapt if behind time
                // ts_next here at most increases to ts_now + 50ms, but at least to ts_now + 50ms - refresh_delay
                ts_next = ts_min - (ts_min - ts_next) % m_interval;
            }

            // increase ts_next to the next fixed test interval
            ts_next += m_interval;
        }
    }
    catch( const std::exception& e )
    {
        std::cerr << "Encountered error during url testing: " << e.what() << std::endl;
    }

    // try clean shutdown
    m_statusTask.shutdown();
}

void UrlWatchTask::cancel()
{
    m_quit = true;
    m_statusTask.cancel();

    if( m_taskRunner.joinable() )
    {
        m_taskRunner.join();
    }

    // lock mutex so the notification can't be missed
    std::lock_guard<std::mutex> lock( m_cvm_timer );
    m_cv_timer.notify_all();
}

void UrlWatchTask::_onStatus( boost::beast::http::status status )
{
    // check status code for whether the url is available
    m_success = status == boost::beast::http::status::ok;
    m_inProcess = false;
    m_cv_timer.notify_all();
}

void UrlWatchTask::_onStatusError( const std::string& error )
{
    m_success = false;
    m_inProcess = false;
    m_cv_timer.notify_all();
}

} // namespace url_check
