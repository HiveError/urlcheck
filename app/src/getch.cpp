#include "getch.hpp"

#ifndef _WIN32
#include <termios.h>    // terminos, tcgetattr, TCSANOW, ICANON, ECHO
#include <unistd.h>     // STDIN_FILENO
#include <cstdio>       // getchar
#endif


namespace url_check {

#ifndef _WIN32
int getch()
{
    termios io_old;
    tcgetattr( STDIN_FILENO, &io_old );
    termios io_new( io_old );
    io_new.c_lflag &= ~(ICANON|ECHO);

    tcsetattr( STDIN_FILENO, TCSANOW, &io_new );
    int c = getchar();
    tcsetattr( STDIN_FILENO, TCSANOW, &io_old );

    return c;
}
#endif

} // namespace url_check
