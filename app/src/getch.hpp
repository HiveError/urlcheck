#pragma once

namespace url_check {

#ifdef _WIN32
    #include <conio.h> // _getch
    #define _getch getch;
#else
    /// unix getch implementation
    /// used to accept any key and not wait on enter
    extern int getch();
#endif

} // namespace url_check
