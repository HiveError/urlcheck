#pragma once

#include "Task.hpp"
#include "WebAddress.hpp"

#include <boost/beast/core.hpp>   // flat_buffer
#include <boost/asio/ip/tcp.hpp>
#include <boost/beast/http.hpp>
#include <boost/beast/http/status.hpp>

#include <functional>
#include <string>


namespace url_check {

    /**
    * \brief UrlStatusTask to retrieve the url status code.
    *        Other than UrlWatchTask this is for a single status request.
    */
    class UrlStatusTask : public Task
    {
    public:
        /**
        * \brief Task constructor initializing it ready for being processed.
        * \param addr           The web address to request the status for.
        * \param cb_status      Callback for successful status retrieval.
        * \param cb_error       Callback for when the status retrieval failed.
        */
        UrlStatusTask( WebAddress addr, std::function<void( boost::beast::http::status )> cb_status, std::function<void( const std::string& )> cb_error );
        virtual ~UrlStatusTask();

        /**
        * \brief issue url status request
        */
        virtual void run() override;

        /**
        * \brief cancel the url status request
        */
        virtual void cancel() override;

        /**
        * \brief shutdown & close socket
        */
        void shutdown();

    private:
        /// \brief callback for when the url ip addresses are resolved or an error occured
        void _onResolve( boost::system::error_code ec, boost::asio::ip::tcp::resolver::results_type results );
        /// \brief callback for when the server is connected or an error occured
        void _onConnect( boost::system::error_code ec );
        /// \brief callback for when the url status request is sent or an error occured
        void _onSend( boost::system::error_code ec, std::size_t bytes_transferred );
        /// \brief callback for when the url status response is received or an error occured
        void _onReceive( boost::system::error_code ec, std::size_t bytes_transferred );

    private:
        WebAddress                                                    m_addr;                 // the url to check
        std::function<void( boost::beast::http::status )>             m_cb_status;            // called with the url status code if retrieved
        std::function<void( const std::string& )>                     m_cb_error;             // called on error

        boost::asio::io_context         m_ioc = boost::asio::io_context( 1 );                 // io context running a single thread
        boost::asio::ip::tcp::resolver  m_resolver = boost::asio::ip::tcp::resolver( m_ioc ); // resolver to resolve the url address
        boost::asio::ip::tcp::socket    m_socket = boost::asio::ip::tcp::socket( m_ioc );     // unopened socket to establish the connection
        boost::beast::flat_buffer       m_buffer;                                             // read buffer
        boost::beast::http::request<boost::beast::http::empty_body>   m_request;              // http head only request for the url status
        boost::beast::http::response<boost::beast::http::string_body> m_response;             // http response message
    };

} // namespace url_check
