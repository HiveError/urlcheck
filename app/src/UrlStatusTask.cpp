#include "UrlStatusTask.hpp"

#include <boost/beast/version.hpp>
#include <boost/asio/connect.hpp>

#include <iostream>           // cout


namespace url_check {

UrlStatusTask::UrlStatusTask( WebAddress addr, std::function<void( boost::beast::http::status )> cb_status, std::function<void( const std::string& )> cb_error )
    : m_addr( std::move( addr ) ), m_cb_status( std::move( cb_status ) ), m_cb_error( std::move( cb_error ) )
{
    // setup general request
    m_request.version( 11 );
    m_request.method( boost::beast::http::verb::get );
    m_request.target( !m_addr.target.empty() ? m_addr.target : "/" );
    m_request.set( boost::beast::http::field::user_agent, BOOST_BEAST_VERSION_STRING );
    m_request.set( boost::beast::http::field::host, m_addr.host );
}

UrlStatusTask::~UrlStatusTask()
{
    // make sure the processing is quit
    cancel();
    shutdown();
}

void UrlStatusTask::run()
{
    // issue url testing
    try
    {
        // initiate url check, attempting to resolve the url address
        m_resolver.async_resolve( m_addr.host, !m_addr.protocol.empty() ? m_addr.protocol : "http",
            std::bind( &UrlStatusTask::_onResolve, this, std::placeholders::_1, std::placeholders::_2 ) );

        // process queued tasks like url resolve, connect, write, read
        m_ioc.run();
    }
    catch( const std::exception& e )
    {
        std::string err = std::string("Failed to issue url status request with error: ") + e.what();
        std::cerr << err << std::endl;
        m_cb_error( err );
    }

    // when run out of work, the io_context needs to be reset prior to the next run call
    m_ioc.restart();
}

void UrlStatusTask::shutdown()
{
    if( !m_socket.is_open() )
    {
        return;
    }

    // try clean shutdown
    try
    {
        // shutdown disabling any further send and receive operations,
        // retrieving the error code, which should be set to boost::asio::error::operation_aborted
        boost::system::error_code ec;
        m_socket.shutdown( boost::asio::ip::tcp::socket::shutdown_both, ec );

        // finally close the socket, also cancelling any outstanding socket connect, send and receive operations
        m_socket.close();
    }
    catch( const std::exception& e )
    {
        std::cerr << "Failed to cleanly shutdown network socket with error: " << e.what() << std::endl;
    }
}

void UrlStatusTask::cancel()
{
    // abort url check
    m_resolver.cancel();

    // cancel all socket connect, send and receive operations
    boost::system::error_code ec;
    m_socket.cancel( ec );
}

void UrlStatusTask::_onResolve( boost::system::error_code ec, boost::asio::ip::tcp::resolver::results_type results )
{
    if( ec )
    {
        // failed to resolve url address
        m_cb_error( "Failed to resolve url with error: " + ec.message() );
        return;
    }

    // connect server, testing all the resolved ip address endpoints
    boost::asio::async_connect( m_socket, results.begin(), results.end(), std::bind( &UrlStatusTask::_onConnect, this, std::placeholders::_1 ) );
}

void UrlStatusTask::_onConnect( boost::system::error_code ec )
{
    if( ec )
    {
        // failed to connect server, skip if aborted
        if( ec != boost::asio::error::operation_aborted )
        {
            m_cb_error( "Failed to connect url with error: " + ec.message() );
        }
        return;
    }

    // send http request
    boost::beast::http::async_write( m_socket, m_request, std::bind( &UrlStatusTask::_onSend, this, std::placeholders::_1, std::placeholders::_2 ) );
}

void UrlStatusTask::_onSend( boost::system::error_code ec, std::size_t bytes_transferred )
{
    if( ec )
    {
        // failed to send request, skip if aborted
        if( ec != boost::asio::error::operation_aborted )
        {
            m_cb_error( "Failed to send request with error: " + ec.message() );
        }
        return;
    }

    // receive request response
    boost::beast::http::async_read( m_socket, m_buffer, m_response, std::bind( &UrlStatusTask::_onReceive, this, std::placeholders::_1, std::placeholders::_2 ) );
}

void UrlStatusTask::_onReceive( boost::system::error_code ec, std::size_t bytes_transferred )
{
    if( ec )
    {
        // failed to receive response, skip if aborted
        if( ec != boost::asio::error::operation_aborted )
        {
            m_cb_error( "Failed to receive response with error: " + ec.message() );
        }
        return;
    }

    // check status code for whether the url is available
    auto const code = m_response.result();
    m_cb_status( code );
}

} // namespace url_check
