#include "WebAddress.hpp"


namespace url_check {

void WebAddress::parseUrl( const std::string& url )
{
    // figure url protocol
    size_t pos = url.find( "://" );
    if( pos != std::string::npos )
    {
        protocol = url.substr( 0, pos );
        pos += 3;
    }
    else
    {
        protocol = "";
        pos = 0; // reset to first character
    }

    // figure url host and target
    size_t end = url.find_first_of( '/', pos );
    host = url.substr( pos, end - pos );
    target = end != std::string::npos ? url.substr( end ) : "";
}

std::string WebAddress::toUrl() const
{
    return protocol + (!protocol.empty() ? "://" : "") + host + target;
}

} // namespace url_check
