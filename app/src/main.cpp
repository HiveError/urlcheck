#include "Dict.hpp"
#include "getch.hpp"
#include "Options.hpp"
#include "UrlChecker.hpp"

#include <chrono>
#include <iostream>
#include <thread>

using namespace url_check;


namespace {

void init_lang()
{
    // get system lang
    auto sysLang = Dict::getSystemLanguage();

    // check & update dictionary language
    if( Dict::hasLanguage( sysLang ) )
    {
        Dict::setLanguage( sysLang );
    }

    // \todo try match the available system locales
    auto langCode = Dict::getLangCode();
    try
    {
        // change locale for the std::cout console output
        std::locale::global( std::locale( langCode ) );
    }
    catch( const std::exception& e )
    {
        // for supported languages in linux type "locale -a"
        std::cout << "Language locale \"" << langCode << "\" not supported, error: " << e.what() << std::endl;
    }
}

void update_lang( std::string& lang )
{
    // update lang for the commandline flags
    if( lang != Dict::getLanguage() )
    {
        if( Dict::hasLanguage( lang ) )
        {
            Dict::setLanguage( lang );
            // change locale for the std::cout console output
            std::locale::global( std::locale( Dict::getLangCode() ) );
        }
        else
        {
            lang = Dict::getLanguage();
        }
    }
}

} // anonymous namespace


int main( int argc, char** argv )
{
    try
    {
        init_lang();

        Options opt;
        opt.lang = Dict::getLanguage();
        try
        {
            if( !opt.parse_commanline( argc, argv ) )
            {
                return 1;
            }
        }
        catch( const std::exception& e )
        {
            std::cerr << "Failed to parse commandline with error: " << e.what() << std::endl;
            return 1;
        }

        update_lang( opt.lang );

        std::cout << opt.url << Dict::lookup( "watching_url" ) << std::endl;

        // start monitor the given url
        UrlChecker checker;
        checker.watch( opt.url, opt.timeout, opt.interval, opt.logfile );

        // small fixture to not quit immediately
        std::this_thread::sleep_for( std::chrono::milliseconds( 20 ) );

	    // wait for some key being pressed
        getch();
        std::cout << "stopping..." << std::endl;

        // quit
        checker.stop();
    }
    catch( const std::exception& e )
    {
        std::cerr << "Failed with error: " << e.what() << std::endl;
        return 1;
    }

    return 0;
}
