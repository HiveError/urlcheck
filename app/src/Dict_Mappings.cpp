#include "Dict.hpp"


namespace url_check {

const auto langDE = LanguageMap( "de", "DE", {
    { "watching_url",       " wird überwacht, drücken Sie eine beliebige Taste um das Programm zu beenden..." },
    { "missing_arguments",  "Bitte von der Kommandokonsole aus mit der zu überwachenden Url aufrufen! Optional kann außerdem ein Logdateipfad mitgegeben werden." },
    { "quit_by_key",        "Zum Beenden eine beliebige Taste drücken." },
    { "url_time",           "Uhr" },
    { "url_reachable",      "erreichbar" },
    { "url_not_reachable",  "nicht erreichbar" },
    { "ask_log_overwrite",  "Logdatei existiert bereits, soll diese fortgeführt werden? (y/n)" }
} );

const auto langEN = LanguageMap( "en", "US", {
    { "watching_url",       " is monitored, press any key to exit the program..." },
    { "missing_arguments",  "Please call from command prompt with the url to monitor! Optionally also provide a logfile path." },
    { "quit_by_key",        "Press any key to quit." },
    { "url_time",           "o'clock" },
    { "url_reachable",      "reachable" },
    { "url_not_reachable",  "not reachable" },
    { "ask_log_overwrite",  "Logfile already exists, shall it be continued? (y/n)" }
} );

const LangIndex Dict::m_index = LangIndex( {
    { "de", langDE },
    { "german", langDE },
    { "en", langEN },
    { "american", langEN },
    { "english", langEN }
} );

} // namespace url_check
