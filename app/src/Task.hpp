#pragma once

namespace url_check {

    /**
    * \brief Poor abstract base Task.
    *        Could be used for some more generic implementation processing the different tasks.
    */
    class Task
    {
    public:
        /// virtual destructor for polymorphic destruction
        virtual ~Task() {}

        /// process the task
        virtual void run() = 0;
        /// cancel processing
        virtual void cancel() = 0;
    };

} // namespace url_check
