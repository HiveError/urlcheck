#include "Dict.hpp"

#include <algorithm>    // transform
#include <cctype>       // tolower
#include <locale>


namespace url_check {

std::string LanguageMap::lookup( const std::string& key ) const
{
    auto it = find( key );
    return it != end() ? it->second : "";
}
    
std::string LangIndex::getLangCode( const std::string& lang ) const
{
    auto it = find( lang );
    std::string langCode;
    if( it != end() )
    {
        langCode = it->second.language;
#ifdef _WIN32
        langCode += '-';
#else
        langCode += '_';
#endif
        langCode += it->second.country;

        // should match the file encoding
        // linux doesn't have the CP-1252 / Windows-1252 charset by default
        // further it is picky about to match the exact naming
        // \todo improve charset selection, checking the available charsets
        //       or possibly embedding the required one to the executable
        langCode += ".UTF-8";
    }
    return langCode;
}

std::string LangIndex::lookup( const std::string& lang, const std::string& key ) const
{
    // search language mappings
    auto it = find( lang );
    if( it != end() )
    {
        auto res = it->second.lookup( key );
        if( !res.empty() )
        {
            return res;
        }
    }
    return "";
}

std::string Dict::m_default_lang = "en";
std::string Dict::m_lang = m_default_lang;

std::string Dict::setLanguage( std::string lang )
{
    std::swap( m_lang, lang );
    return lang;
}

std::string Dict::getLangCode( const std::string& lang )
{
    auto res = m_index.getLangCode( lang );
    {
        // fallback to the default language
        res = m_index.getLangCode( lang );
    }
    return res;
}

std::string Dict::lookup( const std::string& lang, const std::string& key )
{
    auto res = m_index.lookup( lang, key );
    if( res.empty() && m_lang != m_default_lang )
    {
        // fallback to the default language
        res = m_index.lookup( m_default_lang, key );
    }
    return res.empty() ? "<" + key + ">" : res;
}

std::string Dict::getSystemLanguage()
{
    // change global locale to the system locale, by default it is initialized to the 'C' locale
    std::locale orig = std::locale::global( std::locale( "" ) ); // calls setlocale( LC_ALL, "" );
    std::string system_locale = setlocale( LC_ALL, NULL );

    // reset to previous locale
    std::locale::global( orig );

    // strip region & language code
    size_t pos = system_locale.find_first_of( "_-." );
    std::string lang = system_locale.substr( 0, pos );

    // convert to lower case
    std::transform( lang.begin(), lang.end(), lang.begin(), []( char c ) { return static_cast<char>(std::tolower( c )); } );

    return lang;
}

} // namespace url_check
